<?php
header('Content-type: application/json');

require 'vendor/autoload.php';
use PHPHtmlParser\Dom;

$base_url = GetCurrentUri();
$temp_routes = array();
$temp_routes = explode('/', $base_url);
$routes = array();

foreach($temp_routes as $route)
{
    if(trim($route) != '')
    {
        array_push($routes, $route);
    }
}

if(count($routes) > 0)
{
    switch ($routes[0])
    {
        case 'topfiveteams':
            GetTopFiveTeams();
            break;
        case 'toptwentyteams':
            GetTopTwentyTeams();
            break;
        case 'topplayersbystats':
            GetTopPlayersByStats();
            break;
        case 'topfiveplayers':
            GetTopFivePlayers();
            break;
        case 'getplayers':
            GetPlayersByTeamId($routes[1]);
            break;
        case 'getteam':
            GetTeamById($routes[1]);
            break;
        case 'matches':
            GetMatches();
    }
}

function GetCurrentUri()
{
    $basepath = implode('/', array_slice(explode('/', $_SERVER['SCRIPT_NAME']), 0, -1)) . '/';
    $uri = substr($_SERVER['REQUEST_URI'], strlen($basepath));
    if (strstr($uri, '?')) $uri = substr($uri, 0, strpos($uri, '?'));
    $uri = '/' . trim($uri, '/');
    return $uri;
}

// Get current top five teams from HLTV homepage
function GetTopFiveTeams()
{
    $team_names = array();

    $dom = new Dom;
    $dom->load('http://www.hltv.org/');

    $top_five = $dom->find('.vsbox');

    $count = count($top_five);
    for($i = $count - 7; $i < $count - 2; $i++)
    {
        $team_names[] = trim($top_five[$i]->find('a')[0]->find('div')[1]->text);
    }

    echo json_encode($team_names, JSON_PRETTY_PRINT);
}

// Get current top twenty teams
function GetTopTwentyTeams()
{
    $teamlist = array();

    $dom = new Dom;
    $dom->load('http://www.hltv.org/ranking/teams/');

    $teams = $dom->find('.ranking-box');

    foreach($teams as $team)
    {
        $rank_matches;
        $rank_points_string = $team->find('.ranking-teamName > span')[0]->text;
        preg_match('!\d+!', $rank_points_string, $rank_matches);

        $id_matches;
        $rank_delta_element = $team->find('.ranking-delta')[0];
        preg_match('!\d+!', $rank_delta_element->getAttribute('id'), $id_matches);

        $players = array();

        $player_divs = $team->find('.ranking-lineup > div');
        foreach($player_divs as $player_div)
        {
            $player_anchor = $player_div->find('.ranking-playerNick > a')[0];
            $player_link = $player_anchor->getAttribute('href');
            
            $player_id = _getPlayerIdFromLink($player_link);

            $player = array('name' => trim($player_anchor->text),
                'player-id' => $player_id);

            array_push($players, $player);
        }

        $newteam = array('name' => $team->find('.ranking-teamName > a')[0]->text,
            'rank' => intval(substr($team->find('.ranking-number')[0]->text, 1)),
            'rank-points' => intval($rank_matches[0]),
            'rank-delta' => intval($rank_delta_element->text),
            'team-id' => intval($id_matches[0]),
            'team-players' => $players);

        array_push($teamlist, $newteam);
    }

    echo json_encode($teamlist, JSON_PRETTY_PRINT);
}

function GetTopFivePlayers()
{
    $dom = new Dom;
    $dom->load('http://www.hltv.org/');
    
    $top_twenty_div = $dom->find('div.centerFade')[0];
    $player_anchors = $top_twenty_div->find('a');

    $players = array();

    foreach($player_anchors as $player_anchor)
    {
        $player = array();
        $player_link = $player_anchor->getAttribute('href');
        
        $player['name'] = $player_anchor->find('strong')[0]->text;
        $player['player-id'] = _getPlayerIdFromLink($player_link);

        array_push($players, $player);
    }

    echo json_encode($players, JSON_PRETTY_PRINT);
}

// Get top players based on statistics
function GetTopPlayersByStats()
{
    $dom = new Dom;
    $dom->load('http://www.hltv.org/?pageid=348&statsfilter=10&mapid=0');

    $boxes = $dom->find('.framedBox');
    $top_player_categories = array();

    foreach($boxes as $box)
    {
        $category = array();
        $players = array();

        $category['category'] = $box->find('h2')[0]->text;

        $player_elements = $box->find('div');
        foreach($player_elements as $player_element)
        {
            $player = array();
            $player_anchor = $player_element->find('a')[0];
            $player_link = $player_anchor->getAttribute('href');
            $team_name = trim(preg_replace('/\(|\)/','',$player_element->firstChild()->text));

            $stat_value = $player_element->find('div')[1]->text;
            if($stat_value == '')
            {
                $stat_value = $player_element->find('div')[1]->find('span')[0]->text;
            }

            $player['name'] = trim($player_anchor->text);
            $player['player-id'] = _getPlayerIdFromLink($player_link);
            $player['stat'] = $stat_value;
            $player['team'] = $team_name;

            array_push($players, $player);
        }

        $category['players'] = $players;
        array_push($top_player_categories, $category);
    }

    echo json_encode($top_player_categories, JSON_PRETTY_PRINT);
}

// Return players for a team
function GetPlayersByTeamId($teamId)
{
    $dom = new Dom;
    $dom->load('http://www.hltv.org/?pageid=362&teamid=' . $teamId);

    $playerslist = array();

    $titlebox = $dom->find('.centerFade')[0];
    $player_link_elements = $titlebox->find('a');

    for($i = 0; $i < 5; $i++)
    {
        $player_link = $player_link_elements[$i]->getAttribute('href');
        $player = array('name' => trim($player_link_elements[$i]->find('div > div')[1]->text),
            'player-id' => _getPlayerIdFromLink($player_link));
        array_push($playerslist, $player);
    }

    echo json_encode($playerslist, JSON_PRETTY_PRINT);
}

function GetTeamById($teamId)
{
    $dom = new Dom;
    $dom->load('http://www.hltv.org/?pageid=179&teamid=' . $teamId);

    $team_info = array();

    $content_boxes = $dom->find('.covGroupBoxContent');

    $team_info['team-name'] = trim($content_boxes[0]->find('.covSmallHeadline')[0]->text);
    $team_info['region'] = $content_boxes[0]->find('.covSmallHeadline')[3]->text;

    $current_lineup_div = $content_boxes[1];
    $team_info['current-lineup'] = _getLineup($current_lineup_div->find('a'));
    $historical_players_div = $content_boxes[2];
    $team_info['historical-players'] = _getLineup($historical_players_div->find('a'));

    $team_stats_div = $content_boxes[3];
    $teams_stats = array();

    $stat_divs = $team_stats_div->find('.covSmallHeadline');
    for($i = 1; $i < count($stat_divs); $i += 2)
    {
        $stat_title = trim($stat_divs[$i]->text);
        $stat_value = trim($stat_divs[$i + 1]->text);
        $team_stats[$stat_title] = $stat_value;
    }

    $team_info['stats'] = $team_stats;

    echo json_encode($team_info, JSON_PRETTY_PRINT); 
}

function GetMatches()
{
    $dom = new Dom;
    $dom->loadFromUrl('http://www.hltv.org/matches/');

    $matches = array();

    $matches_div = $dom->find('.centerFade');

    $datestring = "";
    $child = $matches_div->firstChild();

    while($child != null)
    {
        $classes = $child->getAttribute('class');
        if(strpos($classes, 'matchListDateBox') !==  false)
        {
            $datestring = $child->text;
        }
        else if(strpos($classes, 'matchListBox') !==  false)
        {
            $match = array();
            $match['date'] = $datestring;
            
            $match_list_row = $child->find('.matchListRow')[0];
            $team_1_cell = $match_list_row->find('.matchTeam1Cell')[0];
            $team_2_cell = $match_list_row->find('.matchTeam2Cell')[0];
            
            if($team_1_cell != null && $team_2_cell != null)
            {
                $team1 = array();
                $team_1_anchor = $team_1_cell->firstChild();
                $team_1_link = $team_1_anchor->getAttribute('href');
                preg_match_all('!\d+!', $team_1_link, $link_matches);
                $team1['name'] = trim($team_1_anchor->text);
                $team1['id'] = intval($link_matches[0][1]);
                $team1['name'] = trim($team_1_anchor->text);
                $match['team1'] = $team1;

                $team2 = array();
                $team_2_anchor = $team_2_cell->find('a');
                $team_2_link = $team_2_anchor->getAttribute('href');
                preg_match_all('!\d+!', $team_2_link, $link_matches);
                $team2['name'] = trim($team_2_anchor->text);
                $team2['id'] = intval($link_matches[0][1]);
                $match['team2'] = $team2;

                array_push($matches,$match);
            }
        }

        try
        {
            $child = $child->nextSibling();
        }
        catch(Exception $e)
        {
            $child = null;
        }
    }

    echo json_encode($matches, JSON_PRETTY_PRINT);
}

function _getLineup($player_anchors)
{
    $players = array();

    foreach($player_anchors as $player_anchor)
    {
        $player = array();
        $player_link = $player_anchor->getAttribute('href');
        $player_text = $player_anchor->text;

        preg_match('!^[^\(]+!', $player_text, $name_matches);
        preg_match('!\(([^)]+)\)!', $player_text, $map_matches);
        preg_match('!\d+!', $map_matches[0], $map_matches);

        $player['player-id'] = _getPlayerIdFromLink($player_link);
        $player['name'] = trim($name_matches[0]);
        $player['maps-played'] = intval($map_matches[0]);

        array_push($players, $player);
    }

    return $players;
}

function _getPlayerIdFromLink($player_link)
{
    $player_id = -1;
    $player_id_matches;
    preg_match_all('!\d+!', $player_link, $player_id_matches);
    
    if(strpos($player_link, 'pageid') !== false)
    {
        $player_id = intval($player_id_matches[0][1]);
    }
    else
    {
        $player_id = intval($player_id_matches[0][0]);
    }

    return $player_id;
}